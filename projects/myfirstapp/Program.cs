﻿using System;

namespace myfirstapp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int yy = add(4,5);
            Console.WriteLine(yy);
            Console.WriteLine(IsOdd(5));
        }

        public static int add(int x, int y)
        {
            return x + y;
        }

        public static bool IsOdd(int value)
        {
            return value % 2 == 1;
        }
    }
}
