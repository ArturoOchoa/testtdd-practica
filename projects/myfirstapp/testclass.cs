using Xunit;

namespace myfirstapp
{
public class testclass{

    [Theory]
    [InlineData(3)]
    [InlineData(5)]
    [InlineData(6)]
    public void MyFirstTheory(int mynumber )
    {
        Assert.True(Program.IsOdd(mynumber));
    }

    [Fact]    
    public void passinAddtest()
    {
        Assert.Equal(4, Program.add(2,2));
    }
    [Fact]    
    public void FailingTest()
    {
        Assert.NotEqual(5, Program.add(2,2));
    }
}

}